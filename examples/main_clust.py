import pandas as pd
import numpy as np
import pickle as pkl
import time
import itertools
import os
import shutil
import collections
import sys
from pathlib import Path
#
from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import StandardScaler
#
import seaborn as sns
import matplotlib.pyplot as plt
#
import torch
from torch import nn
#
sys.path.append("..")
from src.dcmtf import dcmtf


# True - if the model has to be trained. False - Load the previously 'trained model' from the out_dir
is_train = True
# True - Loads previously trained model's 'initializations' - so that we can retrain the model. False - Do new random inits
# True setting is useful, if you find a set of hyperparams & inits doing well and you want to start with that in inits, with some changes to the hyperparams.
is_load_model = False
# True - to pick the previous trained model and start training from that
is_train_prev_model = False

assert len(sys.argv) <= 2," Usage: python main_syn1_clust.py [load_init|no_train|use_pre_train]"

allowed_opts_list = ["load_init","no_train","use_pre_train"]
if len(sys.argv) == 2:
    assert str(sys.argv[1]) in allowed_opts_list, "Unknown option: "+str(sys.argv[1])+". Allowed options:"+str(allowed_opts_list)
    if str(sys.argv[1]) == "load_init":
        is_load_model = True
    if str(sys.argv[1]) == "no_train":
        is_train = False
    if str(sys.argv[1]) == "use_pre_train":
        is_train_prev_model = True

# > 1 if we plan to do multiple runs of this experiments and report avg result
# WARNING: If changed, it has to be changed accordingly in the result gen script.
num_runs = 1

#input dir and data 
data_dir = "data/"
data_dict_fname = "dict_ade_syn_data_v23.pkl"

#output dir
out_base_dir = "out/"
dirpath = Path(out_base_dir)
if not dirpath.exists():
    os.mkdir(dirpath)

#load the data and preprocess
data_dict = pkl.load(open(data_dir+data_dict_fname,'rb'))

dict_mat = data_dict["matrices_data_perm"]

#pp_scaler = Normalizer()
pp_scaler = StandardScaler()
#pp_scaler = MinMaxScaler()
#pp_scaler = MaxAbsScaler()

X_pt = pp_scaler.fit_transform(dict_mat["mat_pat_dis_treat_perm"])
X_pr = pp_scaler.fit_transform(dict_mat["mat_pat_drugs_perm"])
X_rs = pp_scaler.fit_transform(dict_mat["mat_dis_side_drugs_unknown_perm"].T)

print("X_pt.shape: ",X_pt.shape)
print("X_pr.shape: ",X_pr.shape)
print("X_rs.shape: ",X_rs.shape)
print("#")
print("X_pt:")
print("min: ",X_pt.min()," max: ",X_pt.max())
print("X_pr:")
print("min: ",X_pr.min()," max: ",X_pr.max())
print("X_rs:")
print("min: ",X_rs.min()," max: ",X_rs.max())

#Here we construct the data structures required as input to the dcmtf API
#### *entity matrix relationship graph *
#- **G**: dict, keys are entity IDs and values are lists of associated matrix IDs
#### * training data*
#- **X_data**: dict, keys are matrix IDs and values are (1) np.array, or (2) dict
#- **X_meta**: dict, keys are matrix IDs and values are lists of the 2 associated entity IDs

G = {
    "p":["X_pt","X_pr"],\
    "s":["X_rs"],\
    "t":["X_pt"],\
    "r":["X_pr","X_rs"]}


X_data = {
    "X_pt":X_pt, \
    "X_pr":X_pr, \
    "X_rs":X_rs}

X_meta = {
    "X_pt":["p","t"],
    "X_pr":["p","r"],
    "X_rs":["r","s"]
}

#number of clusters to find in each entity
num_clusters = {'p': 4, 's': 4, 't': 4, 'r': 4} 

#### *dCMF network construction - hyperparameters*
# - **kf**: float, in the range (0,1) 
# - **k**: int, entity representation or encoding size. Refer Appendix A in the [paper](https://arxiv.org/abs/1811.11427) for info about how k and kf are used in the dCMF network construction. Same logic applies here.
# - **e_actf**: str, autoencoder's encoding activation function.
# - **d_actf**: str, autoencoder's decoding activation function. Supported functions are "tanh","sigma","relu","lrelu"
# - **is_linear_last_enc_layer**: bool, True to set linear activation for the bottleneck/encoding generation layer 
# - **is_linear_last_dec_layer**: bool, True to set linear activation for the output/decoding generation layer 
# - **num_chunks**: int, number of training batches to create.

kf = 0.000001
k = 100
e_actf = "relu" 
d_actf = "relu" 

learning_rate = 1e-3
weight_decay = 0.00001
max_epochs = 15 #000
convg_thres = 1e-5

#set to false if no gpu
is_gpu = True
#the gpu id to use
gpu_ids = "0"

for cur_run in np.arange(num_runs):
    print("cur_run: ",cur_run)
    out_dir = out_base_dir+"run_"+str(cur_run)+"/"
    print("out_dir: ",out_dir)
    #if not is_load_model and is_train:
    if is_train is True and is_train_prev_model is False and not is_load_model:
        dirpath = Path(out_dir)
        if dirpath.exists() and dirpath.is_dir():
            shutil.rmtree(dirpath)
        os.mkdir(dirpath)
    print("#")
    
    model_dir = out_dir
    
    dcmtf_model = dcmtf(G, X_data, X_meta,\
            k=k, kf=kf, e_actf=e_actf,\
            learning_rate=learning_rate, weight_decay=weight_decay, convg_thres=convg_thres,\
            max_epochs=max_epochs, num_clusters=num_clusters,\
            is_gpu=is_gpu,gpu_ids=gpu_ids,\
            model_dir=model_dir, is_load_model=is_load_model, is_train=is_train,\
            is_train_prev_model = is_train_prev_model)


    dcmtf_model.fit()

    #Persist results
    print("#")
    print("Persisting:")
    print("#")
    #Completed matrices
    for x_id in X_data.keys():
        fname = out_dir+str(x_id)+"_prime.pkl"
        print(fname)
        pkl.dump(dcmtf_model.out_dict_X_prime[x_id].data.cpu().numpy(),open(fname,"wb"))
    print("#")

    #Cluster Indicator matrices
    for e_id in G.keys():
        fname = out_dir+"I_"+str(e_id)+".pkl"
        print(fname)
        pkl.dump(dcmtf_model.out_dict_I[e_id].data.cpu().numpy(),open(fname,"wb"))
    print("#")

    #Entity representation matrices
    for e_id in G.keys():
        fname = out_dir+"U_"+str(e_id)+".pkl"
        print(fname)
        pkl.dump(dcmtf_model.out_dict_U[e_id].data.cpu().numpy(),open(fname,"wb"))
    print("#")

    #entity cluster factor W (where cluster Association matrices A_e1_e2 = W_e1 * W_e2)
    for e_id in G.keys():
        fname = out_dir+"W_"+str(e_id)+".pkl"
        print(fname)
        pkl.dump(dcmtf_model.out_dict_W[e_id].data.cpu().numpy(),open(fname,"wb"))
    print("#")

    #cluster Association matrices A_e1_e2 = W_e1 * W_e2
    for x_id in X_data.keys():
        e1_id = X_meta[x_id][0]
        e2_id = X_meta[x_id][1]
        w_e1 = dcmtf_model.out_dict_W[e1_id].data.cpu().numpy()
        w_e2 = dcmtf_model.out_dict_W[e2_id].data.cpu().numpy()
        fname = out_dir+"A_"+str(e1_id)+str(e2_id)+".pkl"
        print(fname)
        pkl.dump(np.dot(w_e1.T,w_e2),open(fname,"wb"))
    print("#")

    print("Run #"+str(cur_run)+" Completed.")

print("###")
print("All "+str(num_runs)+" runs are complete.")