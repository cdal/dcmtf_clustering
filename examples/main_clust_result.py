import pickle as pkl
import sys
import numpy as np
import pandas as pd
import pickle as pkl
import random
import os
import shutil
from pathlib import Path
#
from sklearn import preprocessing
from sklearn import metrics
from sklearn.metrics import silhouette_score
#
import scipy.stats as ss
from scipy.spatial.distance import cosine,seuclidean,euclidean
#
sys.path.append("..")

#Ensure that the below are set the same values as in main clustering script
# normalizer/scaler: pp_scaler
# version of data: data_dict_path
# In/Output Directory: out_dir, in_dname
# num_clusters
# G, X_data,X_meta
# num_runs

num_runs = 1

#input dir and data 
in_dname = "data/"
data_dict_path = in_dname+"dict_ade_syn_data_v23.pkl"

#output dir
out_base_dir = "out/"

#load the data and normalize
data_dict = pkl.load(open(data_dict_path,'rb'))
dict_mat = data_dict["matrices_data_perm"]

#pp_scaler = preprocessing.Normalizer()
pp_scaler = preprocessing.StandardScaler()
#pp_scaler = preprocessing.MinMaxScaler()
#pp_scaler = preprocessing.MaxAbsScaler()

X_pt = pp_scaler.fit_transform(dict_mat["mat_pat_dis_treat_perm"])
X_pr = pp_scaler.fit_transform(dict_mat["mat_pat_drugs_perm"])
X_rs = pp_scaler.fit_transform(dict_mat["mat_dis_side_drugs_unknown_perm"].T)

print("X_pt.shape: ",X_pt.shape)
print("X_pr.shape: ",X_pr.shape)
print("X_rs.shape: ",X_rs.shape)
print("#")
print("X_pt:")
print("min: ",X_pt.min()," max: ",X_pt.max())
print("X_pr:")
print("min: ",X_pr.min()," max: ",X_pr.max())
print("X_rs:")
print("min: ",X_rs.min()," max: ",X_rs.max())

# 
G = {
    "p":["X_pt","X_pr"],\
    "s":["X_rs"],\
    "t":["X_pt"],\
    "r":["X_pr","X_rs"]}


X_data = {
    "X_pt":X_pt, \
    "X_pr":X_pr, \
    "X_rs":X_rs}

X_meta = {
    "X_pt":["p","t"],
    "X_pr":["p","r"],
    "X_rs":["r","s"]
}

num_clusters = {'p': 4, 's': 4, 't': 4, 'r': 4}

#utilities - start

#Clustering performance

def get_cluster_num_idx(U,num_clusters):
    p_clust_id_list = []
    for i in np.arange(U.shape[0]):
        temp_arr = U[i]
        p_clust_id_list.append(np.argmax(temp_arr))
    #
    p_clust_id_num_dict = {}
    p_clust_id_idx_dict = {}
    for j in np.arange(num_clusters):
        temp_clust_j = np.array(p_clust_id_list) == j
        temp_clust_j_idx = np.arange(U.shape[0])[temp_clust_j]
        num_p = np.sum(temp_clust_j)
        print("cluster: ",j,", #", num_p)
        p_clust_id_num_dict[j] = num_p
        p_clust_id_idx_dict[j] = temp_clust_j_idx     
    return p_clust_id_num_dict, p_clust_id_idx_dict,p_clust_id_list


def get_ari(entity_id,entity_nos,dict_entity_cluster_info,data_dict,which_clust_perm):
    labels_pred = dict_entity_cluster_info[entity_id]["temp_pred_idx"]
    #
    temp_dict = data_dict["cluster_idx"][which_clust_perm]
    labels_true = np.zeros(entity_nos)
    for clust_id in temp_dict.keys():
        temp_idx_list = temp_dict[clust_id]
        for idx in temp_idx_list:
            labels_true[idx] = clust_id
    #
    return metrics.adjusted_rand_score(labels_true, labels_pred) 


def get_nmi(entity_id,entity_nos,dict_entity_cluster_info,data_dict,which_clust_perm):
    labels_pred = dict_entity_cluster_info[entity_id]["temp_pred_idx"]
    #
    temp_dict = data_dict["cluster_idx"][which_clust_perm]
    labels_true = np.zeros(entity_nos)
    for clust_id in temp_dict.keys():
        temp_idx_list = temp_dict[clust_id]
        for idx in temp_idx_list:
            labels_true[idx] = clust_id
    #
    return metrics.normalized_mutual_info_score(labels_true, labels_pred,average_method='arithmetic')  

#utilities - end

dict_overall_result = {}
dict_overall_ari = {}
dict_overall_nmi = {}
dict_overall_sil = {}
for cur_run in np.arange(num_runs):
    print("##################")
    print("cur_run: ",cur_run)
    print("##################")
    out_dir = out_base_dir+"run_"+str(cur_run)+"/"
    print("out_dir: ",out_dir)
    dirpath = Path(out_dir)
    if not dirpath.exists() and not dirpath.is_dir():
        assert False,"Dir does not exists. out_dir: "+out_dir

    #Cluster Idicator matrices
    print("Loading: ")

    dict_I = {}
    for e_id in G.keys():
        fname = out_dir+"I_"+str(e_id)+".pkl"
        print(fname)
        dict_I[e_id] = pkl.load(open(fname,"rb"))

    print("#")

    #cluster Association matrices A_e1_e2 = W_e1 * W_e2
    dict_A = {}
    for x_id in X_data.keys():
        e1_id = X_meta[x_id][0]
        e2_id = X_meta[x_id][1]
        a_id = "A_"+str(e1_id)+str(e2_id)
        fname = out_dir+a_id+".pkl"
        print(fname)
        dict_A[a_id] = pkl.load(open(fname,"rb"))

    print("#")

    #Completed matrices
    dict_X_prime = {}
    for X_id in X_data.keys():
        fname = out_dir+str(X_id)+"_prime.pkl"
        print(fname)
        dict_X_prime[X_id] = pkl.load(open(fname,"rb"))

    print("#")


    #Clustering performance
    #1 - know the cluster info: num datapoints per cluster and cluster datapoint's idxs
    print("#")
    print("#1 Computing cluster info ")
    print("##########################")
    dict_entity_cluster_info = {}
    for entity_id in dict_I.keys():
        print("entity id: ",entity_id)
        I = dict_I[entity_id]
        temp_num, temp_idx, temp_pred_idx = get_cluster_num_idx(I,num_clusters[entity_id])
        dict_entity_cluster_info[entity_id] = {"num":temp_num,"idx":temp_idx,"temp_pred_idx":temp_pred_idx}
        print("#")

    #########  ARI

    dict_e_count = {}
    for e_id in G.keys():
        cur_e_count = 0
        for X_id in G[e_id]:
            if X_meta[X_id][0] == e_id:
                cur_e_count = X_data[X_id].shape[0]
                break
            else:
                cur_e_count = X_data[X_id].shape[1]
                break
            if cur_e_count > 0:
                break
        assert cur_e_count > 0
        dict_e_count[e_id] = cur_e_count 

    dict_e_val_key = {
        "p":"p_clust_perm",
        "r":"r_clust_perm",
        "s":"d_clust_perm",
        "t":"d_clust_perm"
    }

    dict_ari = {}
    for e_id in G.keys():
        cur_ari = get_ari(e_id,dict_e_count[e_id],dict_entity_cluster_info,data_dict,dict_e_val_key[e_id])
        dict_ari[e_id] = cur_ari
    dict_overall_ari[cur_run] = dict_ari

    print("ARI: ")
    for e_id in dict_ari.keys():
        print(e_id," : ",dict_ari[e_id])
    print("#")
    

    #########  NMI

    dict_nmi = {}
    for e_id in G.keys():
        cur_nmi = get_nmi(e_id,dict_e_count[e_id],dict_entity_cluster_info,data_dict,dict_e_val_key[e_id])
        dict_nmi[e_id] = cur_nmi
    dict_overall_nmi[cur_run] = dict_nmi

    print("NMI: ")
    for e_id in dict_nmi.keys():
        print(e_id," : ",dict_nmi[e_id])
    print("#")

    ######### Silhoutte coeff

    dict_sil = {}
    for e_id in G.keys():
        temp_list = []
        for X_id in G[e_id]:
            if X_meta[X_id][0] == e_id:
                temp_list.append(X_data[X_id])
            else:
                temp_list.append(X_data[X_id].T)
        cur_X = np.concatenate(temp_list, axis=1)
        cur_labels = dict_entity_cluster_info[e_id]["temp_pred_idx"]
        assert cur_X.shape[0] == len(cur_labels)
        cur_sil_score = silhouette_score(cur_X,cur_labels,metric="euclidean")
        dict_sil[e_id] = cur_sil_score 
    dict_overall_sil[cur_run] = dict_sil

    print("Silhouette_score: ")
    for e_id in dict_sil.keys():
        print(e_id," : ",dict_sil[e_id])
    print("#")
    print("Run #"+str(cur_run)+" Completed.")
#
print("###")
print("All "+str(num_runs)+" runs are complete.")

if num_runs > 1:
    print("###########################")
    print("Final mean ARI")
    df_temp_ari = pd.DataFrame(dict_overall_ari)
    print(df_temp_ari.mean(axis=1))
    print("---")
    print("sd: ")
    print(df_temp_ari.std(axis=1))
    print("#")
    print("Final mean NMI")
    df_temp_nmi = pd.DataFrame(dict_overall_nmi)
    print(df_temp_nmi.mean(axis=1))
    print("---")
    print("sd: ")
    print(df_temp_nmi.std(axis=1))
    print("#")
    print("Final mean SIL")
    df_temp_sil = pd.DataFrame(dict_overall_sil)
    print(df_temp_sil.mean(axis=1))
    print("---")
    print("sd: ")
    print(df_temp_sil.std(axis=1))

    print("#####################")
    print("all runs: ")
    print("#")
    print("ARI: ")
    print(df_temp_ari)
    print("#")
    print("NMI: ")
    print(df_temp_nmi)
    print("#")
    print("SIL: ")
    print(df_temp_sil)

    print("######################")
    print("ARI:")
    df_res_ari_mean = pd.DataFrame(df_temp_ari.mean(axis=1))
    df_res_ari_mean.columns = ["mean"]
    df_res_ari_std = pd.DataFrame(df_temp_ari.std(axis=1))
    df_res_ari_std.columns = ["std"]
    df_res_ari = df_res_ari_mean.join(df_res_ari_std)
    print(df_res_ari)
    print("#")
    print("NMI:")
    df_res_nmi_mean = pd.DataFrame(df_temp_nmi.mean(axis=1))
    df_res_nmi_mean.columns = ["mean"]
    df_res_nmi_std = pd.DataFrame(df_temp_nmi.std(axis=1))
    df_res_nmi_std.columns = ["std"]
    df_res_nmi = df_res_nmi_mean.join(df_res_nmi_std)
    print(df_res_nmi)
    print("#")
    print("SIL:")
    df_res_sil_mean = pd.DataFrame(df_temp_sil.mean(axis=1))
    df_res_sil_mean.columns = ["mean"]
    df_res_sil_std = pd.DataFrame(df_temp_sil.std(axis=1))
    df_res_sil_std.columns = ["std"]
    df_res_sil = df_res_sil_mean.join(df_res_sil_std)
    print(df_res_sil)