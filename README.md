# dCMTF 

dCMTF is a software package to perform [Deep Collective Matrix Tri-Factorization for Augmented Multi-View Multi-way Clustering](tbd). 

## dCMTF prerequisite
- Python 3.6 
- PyTorch 0.4.0
- NumPy 1.14.0
- SciPy 1.0.0
- scikit-learn 0.19.1

## Running dCMTF

Example code to run dCMTF 

- examples/main_clust.py

Sample data

- examples/data

Sample output and results computation

- examples/main_clust_result.py

- examples/out
