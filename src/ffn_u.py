import numpy as np
import collections
import sys
#
import torch
from torch import nn

class ffnu(nn.Module): 
  
    def get_actf(self,actf_name):
        if actf_name is "relu":
            A = nn.ReLU()
        elif actf_name is "sigma":
            A = nn.Sigmoid()
        elif actf_name is "tanh":
            A = nn.Tanh()
        elif actf_name is "lrelu":
            A = nn.LeakyReLU()
        elif actf_name is "softmax":
            A = nn.Softmax(dim=1)
        else:
            print("Unknown activation function: ",actf_name)
            sys.exit(1)
        return A

    def __init__(self,input_dim,\
                        k_list,\
                        actf_list):
                        #num_clusters,\
                        #num_uw_out):
        super(ffnu, self).__init__()
        #
        enc_layers_dict = collections.OrderedDict()
        enc_layers_extra_dict_1_clust_id = collections.OrderedDict()
        enc_layers_extra_dict_2_clust_uw = collections.OrderedDict()
        num_enc_layers = len(k_list)
        k1 = input_dim
        for i in np.arange(num_enc_layers):
            k2 = k_list[i]
            enc_layers_dict["enc-"+str(i)] = nn.Linear(int(k1), int(k2))
            enc_layers_dict["act-"+str(i)] = self.get_actf(actf_list[i]) 
            k1 = k2
        #
        self.mu_layer = nn.Linear(int(k2), int(k2),bias=True) #TODO: decide number of output units for mu, sigma
        self.sigma_layer = nn.Linear(int(k2), int(k2),bias=True)
        self.encoder = nn.Sequential(enc_layers_dict)
        #
        print("U: encoder ")
        print(self.encoder)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar) #TODO: 0.5?
        eps = torch.randn_like(std)
        return mu + eps*std

    def forward(self, x):
        x_enc_prev = self.encoder(x)
        mu, logvar = self.mu_layer(x_enc_prev), self.sigma_layer(x_enc_prev)
        x_enc = self.reparameterize(mu, logvar)
        return x_enc, mu, logvar
