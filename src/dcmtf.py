import itertools
import numpy as np
import collections
import sys

import operator
import os
import pprint
import scipy
import time

#
import torch
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F

from sklearn.neighbors import kneighbors_graph
from sklearn.metrics import pairwise_distances

from sklearn.preprocessing import MaxAbsScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import StandardScaler

from src.ffn_u import ffnu
from src.ffn_iw import ffniw

class dcmtf(nn.Module):

    def __print_params(self):
        print("#")
        print("dCMF: ")
        print("#")
        print("learning_rate: ",self.learning_rate)
        print("weight_decay: ",self.weight_decay)
        print("convg_thres: ",self.convg_thres)
        print("max_epochs: ",self.max_epochs)
        print("num_clusters: ",self.num_clusters)
        print("k: ",self.k)
        print("kf: ",self.kf)
        print("e_actf: ",self.e_actf)
        print("is_gpu: ",self.is_gpu)
        print("gpu_ids: ",self.gpu_ids)
        print("#")
        print("G:")
        print(self.G) 
        print("X_data:")
        print(self.X_data)
        print("X_meta:")
        print(self.X_meta)
        print("#")

    def __validate_input(self):
        #ensure if expected data types and structures are used
        assert isinstance(self.G, dict), "G must be a dictionary with key: entity ID, value: associated matrix ID"
        assert isinstance(self.X_data, dict), "X_data must be a dictionary with key: matrix ID, value: matrix"
        assert isinstance(self.X_meta, dict), "X_meta must be a dictionary with key: matrix ID, value: list with elements [row_entity_ID, column_entity_ID]"
        #G: ensure all IDs are strings, values are list of strings
        for e_id in self.G.keys():
            assert isinstance(e_id, str), "Entity IDs in G must be of type str. Got ("+str(type(e_id))+")"
        for e_id in self.G.keys():
            assert isinstance(self.G[e_id], list), "G["+str(e_id)+"] must be a list. Got ("+str(type(self.G[e_id]))+")"
            for temp_item in self.G[e_id]:
                assert isinstance(temp_item,str), "G["+str(e_id)+"] must be a list of str. Got ("+str(type(temp_item))+") in the list."
        #X_data: ensure all IDs are string, values are np.array
        for X_id in self.X_data.keys():
            assert isinstance(X_id, str), "Matrix IDs in X_data must be of type str. Got ("+str(type(X_id))+")"
            assert isinstance(self.X_data[X_id],dict) or isinstance(self.X_data[X_id],np.ndarray),"X_data must be either np.ndarray or (if this matrix participates in validation, then) a dict(1) of dict(2), where dict(1)'s keys are matrix id and values are dict(2). The dict(2)'s keys are fold id and values are nd-array"
        #X_meta: ensure all IDs are string, values are list of 2 IDs
        for X_id in self.X_meta.keys():
            assert isinstance(X_id, str), "Matrix IDs in X_meta must be of type str. Got ("+str(type(X_id))+")"
            assert isinstance(self.X_meta[X_id],list), "Values in X_meta must be of type list. Got ("+str(type(self.X_meta[X_id]))+") for matrix ID: "+X_id
            assert len(self.X_meta[X_id]) == 2, "Values in X_meta must be of type list and size 2. Got a list of length ("+str(len(self.X_meta[X_id]))+") for matrix ID: "+X_id
            for temp_item in self.X_meta[X_id]:
                assert isinstance(temp_item, str), "Values in X_meta must be list of str. Got a list with ("+str(type(temp_item))+") item for matrix ID: "+X_id
        #ensure e and X labels used across the data structures are consistent and tally
        G_e_list = list(np.unique(list(self.G.keys())))
        G_x_list = list(np.unique(list(itertools.chain(*list(self.G.values())))))
        X_meta_e_list = list(np.unique(list(itertools.chain(*list(self.X_meta.values())))))
        X_meta_x_list = list(np.unique(list(self.X_meta.keys())))
        X_data_x_list = list(np.unique(list(self.X_data.keys())))
        if G_e_list != X_meta_e_list:
            print("e in G: ",G_e_list)
            print("e in X_meta",X_meta_e_list)
            raise Exception("Entity IDs in G and X_meta should match")   
        if G_x_list != X_meta_x_list or \
            X_meta_x_list != X_data_x_list:
            print("X in G: ",G_x_list)
            print("X in X_meta: ",X_meta_x_list)
            print("X in X_data: ",X_data_x_list)
            raise Exception("Matrix IDs in G, X_data and X_meta should match")

        #these can't be none if is_bo == False
        list_of_mandatory_params = ["k","kf","e_actf","learning_rate","weight_decay","convg_thres", "max_epochs"] 
        for param_name in list_of_mandatory_params:
            f = operator.attrgetter(param_name)
            if f(self) is None:
                assert False, "param: "+param_name+" can't be None."
        #
        assert np.isreal(self.kf)
        assert self.kf > 0 and self.kf < 1, "kf must be in range (0,1)"
        assert isinstance(self.k, int), "k (the encoding length) must be an int"
        assert isinstance(self.e_actf, str), "e_actf must be a str"
        assert np.isreal(self.learning_rate), "learning_rate must be real"
        assert np.isreal(self.weight_decay), "weight_decay must be real"
        assert np.isreal(self.convg_thres), "convg_thres must be real"
        assert isinstance(self.max_epochs, int) or (self.max_epochs == None), "max_epochs can be either None(to run till convergence) or an int"
        assert isinstance(self.num_clusters, dict) or (self.num_clusters == None), "num_clusters has to be a dict"
        assert isinstance(self.is_gpu, bool), "is_gpu can either be True or False"
        assert isinstance(self.gpu_ids, str), "gpu_ids has to be a str like '1' or '1,2' where 1 and 2 are the gpu cuda IDs" 
      
    
    def __get_k_list(self,n,k,kf):
        k_list = []
        while True:
            k1 = int(n * kf)
            if k1 > k:
                k_list.append(k1)
                n = k1
            else:
                k_list.append(k)
                break
        return k_list

    def __get_actf_list(self,k_list,e_actf):
        actf_list_e = []
        for k in k_list:
            actf_list_e.append(e_actf) 
        return actf_list_e


    def __input_transformation(self):
        #for each entity, construct the input matrix to the corresponding network
        #Building C_dict
        print("__input_transformation - start")
        print("#")
        print("concatenated-matrix construction...")
        for e_id in self.G.keys():
            print("e_id: ",e_id)
            X_id_list = self.G[e_id]
            print("X_id_list: ",X_id_list)
            #X_data_list = []
            for X_id in X_id_list:
                print("X_id: ",X_id)
                print("X[X_id].shape: ",self.X_data[X_id].shape)
                cur_id = "e_"+e_id+"_x_"+X_id
                print("cur_id: ",cur_id)
                if self.X_meta[X_id][0] == e_id:
                    #X_data_list.append(self.X_data[X_id])
                    temp = self.X_data[X_id]
                else:
                    #X_data_list.append(self.X_data[X_id].T)
                    temp = self.X_data[X_id].T
                C_temp = Variable(torch.from_numpy(temp).float(),requires_grad=False)
                if self.is_gpu:
                    self.C_dict[cur_id] = C_temp.cuda()
                else:
                    self.C_dict[cur_id] = C_temp
                print("C_dict[e].shape: ",self.C_dict[cur_id].shape)
                print("---")
        print("#")
        print("creating pytorch variables of input matrices...")
        #Convert input matrices to pytorch variables (to calculate reconstruction loss)
        #Building X_data_var
        for X_id in self.X_data.keys():
            X_temp = Variable(torch.from_numpy(self.X_data[X_id]).float(),requires_grad=False)
            if self.is_gpu:
                self.X_data_var[X_id] = X_temp.cuda()
            else:
                self.X_data_var[X_id] = X_temp
        print("#")
        print("__input_transformation - end")


    def __network_construction(self):
        #for each entity construct an autoencoder for every matrix its a part of
        print("__network_construction - start")
        if self.is_train_prev_model:
            fname_model_u = self.model_dir+"N_ffn_u_dict_trained.pt"
            print("Loading: ",fname_model_u)
            self.N_ffn_u_dict = torch.load(fname_model_u)
            fname_model_iw = self.model_dir+"N_ffn_iw_dict_trained.pt"
            print("Loading: ",fname_model_iw)
            self.N_ffn_iw_dict = torch.load(fname_model_iw)
        else:
            if not self.is_load_model: 
                for e_id in self.G.keys():
                    print("###")
                    print("e_id: ",e_id)
                    print("Network U:")
                    e_id_input_dim = 0
                    for x_id in self.G[e_id]:
                        print("x_id: ",x_id)
                        cur_id = "e_"+e_id+"_x_"+x_id
                        print("cur_id: ",cur_id)
                        C = self.C_dict[cur_id]
                        k_list = self.__get_k_list(C.shape[1],self.k,self.kf) 
                        actf_list = self.__get_actf_list(k_list,self.e_actf)
                        ffn_u = ffnu(C.shape[1],k_list,actf_list)
                        print("C.shape: ")
                        print(C.shape)
                        print("ffn_u: ")
                        print(ffn_u)
                        if self.is_gpu:
                            ffn_u.cuda()
                        self.N_ffn_u_dict[cur_id] = ffn_u
                        e_id_input_dim+=k_list[len(k_list)-1]
                        print("#")
                    print("Network IW:")
                    print("e_id_input_dim: ",e_id_input_dim)
                    ffn_iw = ffniw(e_id_input_dim, self.num_clusters[e_id],\
                                            np.min(list(self.num_clusters.values())))
                    if self.is_gpu:
                        ffn_iw.cuda()
                    print("ffn_iw: ")
                    print(ffn_iw)
                    self.N_ffn_iw_dict[e_id] = ffn_iw
                    print("###")
                #
                fname_model_u = self.model_dir+"N_ffn_u_dict.pt"
                print("Persisting: ",fname_model_u)
                torch.save(self.N_ffn_u_dict,fname_model_u)
                fname_model_iw = self.model_dir+"N_ffn_iw_dict.pt"
                print("Persisting: ",fname_model_iw)
                torch.save(self.N_ffn_iw_dict,fname_model_iw)
            else :
                fname_model_u = self.model_dir+"N_ffn_u_dict.pt"
                print("Loading: ",fname_model_u)
                self.N_ffn_u_dict = torch.load(fname_model_u)
                fname_model_iw = self.model_dir+"N_ffn_iw_dict.pt"
                print("Loading: ",fname_model_iw)
                self.N_ffn_iw_dict = torch.load(fname_model_iw)
        print("__network_construction - end")


    def __init__(self, G, X_data, X_meta,\
            k, kf, e_actf,\
            learning_rate, weight_decay, convg_thres,\
            max_epochs, num_clusters,\
            is_gpu, gpu_ids, model_dir, is_load_model, is_train,\
            is_train_prev_model):
        #
        super(dcmtf, self).__init__()
        #
        self.model_dir = model_dir
        self.is_load_model = is_load_model
        self.is_train = is_train
        self.is_train_prev_model = is_train_prev_model
        #
        self.G = G 
        self.X_data = X_data
        self.X_meta = X_meta
        self.k = k
        self.kf = kf
        self.e_actf = e_actf
        self.learning_rate = learning_rate
        self.weight_decay = weight_decay
        self.convg_thres = convg_thres
        self.max_epochs = max_epochs
        self.num_clusters = num_clusters
        self.is_gpu = is_gpu
        self.gpu_ids = gpu_ids
        torch.cuda.set_device(int(self.gpu_ids))
        #
        self.__validate_input()
        self.__print_params()
        #out
        self.out_dict_U = {}
        self.out_dict_IW = {}
        self.out_dict_I = {}
        self.out_dict_W = {}
        self.out_dict_X_prime = {}
        #dcmtf model construction
        print("dcmtf - model construction - start")
        self.C_dict = {}
        self.X_data_var = {}
        self.__input_transformation()
        self.N_ffn_u_dict = {}
        self.N_ffn_iw_dict = {}
        if self.is_train:
            self.__network_construction()
        print("dcmtf - model construction - end")
        print("#")

    def __is_converged(self,prev_cost,cost,convg_thres):
        #diff = (prev_cost - cost)
        diff = (abs(prev_cost) - abs(cost))
        if (abs(diff)) < convg_thres:
            return True

    def __cosine_similarity_thresholded(self,x1, x2=None, eps=1e-8):
        #based on https://github.com/pytorch/pytorch/issues/11202
        x2 = x1 if x2 is None else x2
        w1 = x1.norm(p=2, dim=1, keepdim=True)
        w2 = w1 if x2 is x1 else x2.norm(p=2, dim=1, keepdim=True)
        temp = torch.mm(x1, x2.t()) / (w1 * w2.t()).clamp(min=eps)
        temp[temp <= 0] = 0
        return temp

    def fit(self):
        print("#")
        print("dcmf.fit - start")
        if self.is_train:
            print("Training started.")
            #load all the params
            model_params = []
            for e_id in self.G.keys():
                for x_id in self.G[e_id]:
                    cur_id = "e_"+e_id+"_x_"+x_id
                    params_temp = list(self.N_ffn_u_dict[cur_id].parameters())
                    #print("aec for e_id: ",e_id,", #params: ",len(params_temp))
                    model_params+=params_temp
            for e_id in self.G.keys():
                params_temp = list(self.N_ffn_iw_dict[e_id].parameters())
                model_params+=params_temp
            #create the optimizer
            optimizer = torch.optim.Adam(model_params, lr=self.learning_rate, weight_decay=self.weight_decay)
            #
            self.dict_epoch_loss = {}
            self.dict_epoch_loss_clust = {}
            self.dict_epoch_loss_local = {}
            epoch = 1
            prev_loss_epoch = 0
            while True:
                if epoch > self.max_epochs:
                    break
                #epoch - start
                s = time.time()
                #
                loss_epoch = 0
                loss_epoch_clust = 0
                loss_epoch_local = 0
                #
                dict_e_x_id_U = {}
                for e_id in self.G.keys():
                    temp_list = []
                    for x_id in self.G[e_id]:
                        cur_id = "e_"+e_id+"_x_"+x_id
                        #
                        temp_U, _ , _ = self.N_ffn_u_dict[cur_id](self.C_dict[cur_id]) #TODO: construct N_ffn_u_dict
                        temp_list.append(temp_U)
                        dict_e_x_id_U[cur_id] = temp_U
                    self.out_dict_U[e_id] = torch.cat(temp_list,dim=1)
                #
                for e_id in self.G.keys():
                    self.out_dict_I[e_id], self.out_dict_IW[e_id], self.out_dict_W[e_id] = self.N_ffn_iw_dict[e_id](self.out_dict_U[e_id]) #TODO: construct N_ffn_iw_dict

                #reconstrution loss
                dict_loss_clust = {}
                for x_id in self.X_meta.keys():
                    row_e_id = self.X_meta[x_id][0]
                    row_IW = self.out_dict_IW[row_e_id]
                    #
                    col_e_id = self.X_meta[x_id][1]
                    col_IW = self.out_dict_IW[col_e_id]
                    #
                    X_orig = self.X_data_var[x_id]
                    X_rec = row_IW.mm(col_IW.transpose(1,0))
                    assert X_orig.shape[0] == X_rec.shape[0]
                    assert X_orig.shape[1] == X_rec.shape[1]
                    self.out_dict_X_prime[x_id] = X_rec
                    dict_loss_clust[x_id] = torch.norm((X_rec-X_orig)[X_orig > 0])
                    dict_loss_clust[x_id].backward(retain_graph=True)

                #locality preserving loss 
                #print("#")
                dict_loss_local = {}
                for e_id in self.G.keys():
                    ##print("e_id: ",e_id)
                    for x_id in self.G[e_id]:
                        cur_id = "e_"+e_id+"_x_"+x_id
                        ##print("cur_id: ",cur_id)
                        U_temp = dict_e_x_id_U[cur_id]
                        ##print("U_temp.shape: ",U_temp.shape)
                        if self.X_meta[x_id][0] is e_id:
                            ##print("Transposed: No")
                            X_temp = torch.from_numpy(self.X_data[x_id]).float().cuda()
                        else:
                            X_temp = torch.from_numpy(self.X_data[x_id].T).float().cuda()
                            ##print("Transposed: Yes")
                        #W
                        W_topk = self.__cosine_similarity_thresholded(X_temp)#, X_temp)
                        #Q
                        Q_temp = torch.diag(torch.sum(W_topk,dim=0))
                        ##print("Q_temp.shape: ",Q_temp.shape)
                        dict_loss_local[cur_id] = 0.001 * torch.trace(U_temp.transpose(1,0).mm(Q_temp-W_topk).mm(U_temp))
                        #print("dict_loss_local[cur_id]: ",dict_loss_local[cur_id])
                        dict_loss_local[cur_id].backward(retain_graph=True)
                    ##print("#")
                #
                loss_epoch_clust = np.sum(list(dict_loss_clust.values()))
                loss_epoch_local = np.sum(list(dict_loss_local.values()))
                loss_epoch = loss_epoch_clust + loss_epoch_local
                #
                optimizer.step()
                optimizer.zero_grad()
                #
                self.dict_epoch_loss[epoch] = loss_epoch.item()
                self.dict_epoch_loss_clust[epoch] = loss_epoch_clust.item()
                self.dict_epoch_loss_local[epoch] = loss_epoch_local.item()
                #epoch - end
                e = time.time()
                print("epoch: ",epoch," total loss: ",loss_epoch.item(),", clust: ",loss_epoch_clust.item(),", local: ",loss_epoch_local.item(),". Took ",round(e-s,1)," secs.")
                #update - counter
                epoch+=1
                if self.__is_converged(prev_loss_epoch,loss_epoch,self.convg_thres):
                    print("**train converged**")
                    break
                prev_loss_epoch = loss_epoch

            print("Training complete.")
            fname_model_u = self.model_dir+"N_ffn_u_dict_trained.pt"
            print("Persisting trained model: ",fname_model_u)
            torch.save(self.N_ffn_u_dict,fname_model_u)
            fname_model_iw = self.model_dir+"N_ffn_iw_dict_trained.pt"
            print("Persisting trained model: ",fname_model_iw)
            torch.save(self.N_ffn_iw_dict,fname_model_iw)
            print("#")
        else:
            fname_model_u = self.model_dir+"N_ffn_u_dict_trained.pt"
            print("Loading trained model: ",fname_model_u)
            self.N_ffn_u_dict = torch.load(fname_model_u)
            fname_model_iw = self.model_dir+"N_ffn_iw_dict_trained.pt"
            print("Loading trained model: ",fname_model_iw)
            self.N_ffn_iw_dict = torch.load(fname_model_iw)
            print("Trifactorization - start ")
            for e_id in self.G.keys():
                temp_list = []
                for x_id in self.G[e_id]:
                    cur_id = "e_"+e_id+"_x_"+x_id
                    #
                    temp_U, _ , _ = self.N_ffn_u_dict[cur_id](self.C_dict[cur_id])
                    temp_list.append(temp_U)
                self.out_dict_U[e_id] = torch.cat(temp_list,dim=1)
            #
            for e_id in self.G.keys():
                self.out_dict_I[e_id], self.out_dict_IW[e_id], self.out_dict_W[e_id] = self.N_ffn_iw_dict[e_id](self.out_dict_U[e_id]) #TODO: construct N_ffn_iw_dict
            print("Trifactorization - end ")
            #reconstrution using 4 factors
            for x_id in self.X_meta.keys():
                row_e_id = self.X_meta[x_id][0]
                row_IW = self.out_dict_IW[row_e_id]
                #
                col_e_id = self.X_meta[x_id][1]
                col_IW = self.out_dict_IW[col_e_id]
                #
                X_orig = self.X_data_var[x_id]
                X_rec = row_IW.mm(col_IW.transpose(1,0))
                assert X_orig.shape[0] == X_rec.shape[0]
                assert X_orig.shape[1] == X_rec.shape[1]
                self.out_dict_X_prime[x_id] = X_rec
        print("dcmf.fit - end")
        print("#")
