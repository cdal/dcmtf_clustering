import numpy as np
import collections
import sys
#
import torch
from torch import nn

class ffniw(nn.Module): #VAE
  
    def get_actf(self,actf_name):
        if actf_name is "relu":
            A = nn.ReLU()
        elif actf_name is "sigma":
            A = nn.Sigmoid()
        elif actf_name is "tanh":
            A = nn.Tanh()
        elif actf_name is "lrelu":
            A = nn.LeakyReLU()
        elif actf_name is "softmax":
            A = nn.Softmax(dim=1)
        else:
            print("Unknown activation function: ",actf_name)
            sys.exit(1)
        return A

    def __init__(self,input_dim,\
                        num_clusters,\
                        num_uw_out):
        super(ffniw, self).__init__()
        
        enc_layers_extra_dict_1_clust_id = collections.OrderedDict()
        enc_layers_extra_dict_2_clust_uw = collections.OrderedDict()
        #layer 1 - converts reprn k to num_clusters
        enc_layers_extra_dict_1_clust_id["clust1"] = nn.Linear(int(input_dim), int(num_clusters),bias=True)
        enc_layers_extra_dict_1_clust_id["clust1-softmax"] = self.get_actf("softmax")
        #layer 2 - learns the cluster association matrix A ~ W
        enc_layers_extra_dict_2_clust_uw["clust2"] = nn.Linear(int(num_clusters), int(num_uw_out),bias=False) #TODO: decide number of output units
        #know the weight layer id
        self.weight_layer_id = "clust2"
        #create sequence of layers
        self.encoder_extra_layer_1_clust_id = nn.Sequential(enc_layers_extra_dict_1_clust_id)
        self.encoder_extra_layer_2_clust_uw = nn.Sequential(enc_layers_extra_dict_2_clust_uw)

        print("#")
        print("I: cluster indicator layer: ")
        print(self.encoder_extra_layer_1_clust_id)
        print("#")
        print("I * W : trifac weight layer: ")
        print(self.encoder_extra_layer_2_clust_uw)
        print("#")

    def forward(self, x_enc):
        x_enc_clust_I = self.encoder_extra_layer_1_clust_id(x_enc)
        x_enc_clust_IW = self.encoder_extra_layer_2_clust_uw(x_enc_clust_I)
        for temp_module_tup in self.encoder_extra_layer_2_clust_uw.named_children():
            if temp_module_tup[0] == self.weight_layer_id:
                self.W = temp_module_tup[1].weight
        return x_enc_clust_I, x_enc_clust_IW, self.W
